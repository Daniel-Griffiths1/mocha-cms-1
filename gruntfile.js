module.exports = function(grunt) {

  grunt.initConfig({

    /*
    |-------------------------------------------------------
    | Watch
    |-------------------------------------------------------
    | watch for any SCSS file changes 
    |
    */

    watch: {
      css: {
        files: ['**/*.scss'],
        tasks: ['sass'],
        options: {
          spawn: false,
        },
      },
    },

    /*
    |-------------------------------------------------------
    | SASS/SCSS
    |-------------------------------------------------------
    | compile SCSS/SASS to CSS 
    |
    */

    sass: {
      dist: {
        files: {
          'admin/assets/css/main.css': 'admin/assets/scss/main.scss'
        }
      }
    }
    
  });

  /*
  |-------------------------------------------------------
  | Load Tasks
  |-------------------------------------------------------
  */

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');

  /*
  |-------------------------------------------------------
  | Register Tasks
  |-------------------------------------------------------
  */
  
  grunt.registerTask('default', ['watch']);

};