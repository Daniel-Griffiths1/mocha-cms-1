<?php
ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE); 

//put all files into an array
$posts = glob('posts/*.{php}', GLOB_BRACE);

//order by date
usort($posts, create_function('$a,$b', 'return filemtime($b) - filemtime($a);'));

//loop through each file
foreach($posts as $post){
	
$post_formatted = basename($post);
$post_formatted = str_replace('.php','',$post_formatted);

//get the file contents
$post_content = file_get_contents($post);

//if blog post is set to true then extract post values form the file

echo '<section>
<article>
';

$title = array();
preg_match('#<h2[^>]*>(.*?)</h2>#', $post_content, $title);

$content = array();
preg_match('#\\<div id="content"\\>(.+)\\</div\\>#s', $post_content, $content);

$date = array();
preg_match('#<time[^>]*>(.*?)</time>#', $post_content, $date);

//display post
echo  '   <h2>' . strip_tags(reset($title)) . '</h2>
  <time>' . strip_tags(reset($date)) . '</time>
  <p>' . strip_tags(substr(reset($content) , 0, 500)) . '...</p>

  <a class="btn no-margin" href="posts/' . $post_formatted . '.php">Read Article</a>
  <div class="clear"></div>
</article>
</section>
';

}
  
?>
