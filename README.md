# Mocha CMS
A simple flat file content management system.

![alt tag](http://i.imgur.com/2Cfn1AV.png)

Mocha is currently alpha quality software and should not be used in production.

### About
Mocha was created an an experiment to try and create a flat file content managment system. 

### Usage
Make sure you place Mocha inside a `/admin/` folder on the root directory of your website.

To begin using Mocha you might want to start with changing the default username/password, this can be changed in the `/includes/settings.php` file. The settings file also lets you change the tags Mocha looks for when editing pages/posts, this can be left untouched unless you change the default template files (more on that later).

Since Mocha is flat file based it needs to know what the stucture of your websites pages/posts will look like, you can do this by editing the page and post files in the `/templates/` folder. Keep in mind that if you edit the defualt templates then you will also need to update the tags in the settings file.

**Other requirements:-**
* You will need a `/posts/` folder on the root of your site to save any posts (This directory can be changed in the `/includes/settings.php` file)
* Your navigation will need to be split off into a nav.php files and places in an includes folder in the root of your site. Eg. `/includes/nav.php`. When editiing the nav Mocha will replace this with the standard ul li configuration.

Thats about it, should be easy enough to plug into both new and existing websites!

### License
Mocha is open source software that is free to use for non-profit use.