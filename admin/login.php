<?php
/*
* Title: Login Page
* Developer: Daniel Griffiths
* Updated: 2/05/2015
*/

session_start();

include('includes/settings.php');

//check login details
if(isset($_POST['login'])){
  if($_POST['username'] == $username && $_POST['password'] == $password){
    //create session and redirect user to the CMS dashboard
    $_SESSION['logged_in']=true;
    header('location:index.php');
  }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
  <title>Login - <?php echo strip_tags($branding); ?></title>
</head>
<body>
<style>
html{padding:0;margin:0;}

body{
  font-family: 'Open Sans',sans-serif;
  padding: 0;
  margin: 0;
  background: #1C1F26;
  font-size: 12px;
  -webkit-font-smoothing: antialiased;
}

h1{
  margin: 0;
  margin-bottom:10px;
  padding-bottom: 8px;
  font-size: 14px;
  font-weight:bold;
  border-bottom: 1px solid lightgray;
}

form{margin:0;padding:0;text-align:left;display: inline;}

.login-box{
  width: 280px;
  background: #fff;
  margin: auto;
  margin-top: 12%;
  padding: 15px;
  border-radius: 5px;
  border: 5px solid #3D4454;
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  <?php if(!isset($_POST['login'])){
    //we dont want to animate every time the password is wrong
    echo '-webkit-animation: fade-in 1s;
    animation: fade-in 1s';
  }
  ?>
}

input{
  width: 90%;
  color: #555555;
  font-size: 12px;
  padding: 10px 12px;
  margin-bottom: 10px;
  margin-top: 7px;
  border: solid 1px lightgray;
}

.btn{
  padding: 11px 15px 11px 15px;
  border-radius: 3px;
  border:0;
  cursor:pointer;
  margin-top:5px;
  margin-bottom:0;
  width: 100%;
  color: #ffffff;
  background: #777C9B;
  outline-style: none;
  transition: 0.5s all;
}

.btn:hover{
  background: #8489AD;
}

.error{
  color: red;
  width: 280px;
  margin: auto;
  padding: 10px;
  text-align: center;
  font-weight: bold;
}

/* reset input styles on iOS */
input, textarea{
  -webkit-appearance: none;
}

/* animations */
@-webkit-keyframes fade-in {
  from {margin-top: 15%;opacity:0;}
  to {margin-top: 12%;opacity:1;}
}

@keyframes fade-in {
  from {margin-top: 15%;opacity:0;}
  to {margin-top: 12%;opacity:1;}
}
</style>
<div class="login-box">
  <h1>Sign In</h1>
  <form name="input" method="post" autocomplete="on">
    <input type="username" name="username" placeholder="Username">
    <input type="password" name="password" placeholder="Password">
    <input type="submit" name="login" class="btn green" value="Login">
  </form>
</div>
<?php
//display error message if details were incorrect
if(isset($_POST['login'])){
  if($_POST['username'] != $username && $_POST['password'] != $password){
    echo '<div class="error">Username or Password was incorrect</div>';
  }
}
?>
</body>
</html>
