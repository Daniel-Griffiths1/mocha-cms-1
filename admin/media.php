<?php
/*
* Title: Media Manager
* Developer: Daniel Griffiths
* Updated: 27/02/2015
*/

//include main site settings
include('includes/head.php');
include('includes/side.php');
?>
  <section>
    <article>
      <h2>Media Uploads</h2>
      <form enctype="multipart/form-data" action="includes/core/media/upload-media.php" method="post" id="upload-media-form">
        <input name="uploadedfile" type="file" id="upload-media-file" required/>
        <input type="submit" class="btn" value="Upload File" />
      </form>
      <div class="clear"></div>
        <?php $cms->getMedia(); ?>
      </article>
  </section>
<?php
include('includes/foot.php');
?>
