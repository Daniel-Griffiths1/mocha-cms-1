<?php
/*
* Title: List Website Pages
* Developer: Daniel Griffiths
* Updated: 27/02/2015
*/

//include main site settings
include('includes/head.php');
include('includes/side.php');
?>
  <section id="editor">
    <article>
      <h2>Current Pages</h2>
      <a href="new-page.php"><div class="btn"><i class="fa fa-plus"></i> Create New Page</div></a>
      <a href="#"><div id="delete-pages" class="btn red"><i class="fa fa-times"></i> Delete Selected Pages</div></a>
      <div class="clear"></div>
      <div class="pages">
        <?php $cms->getPages(); ?>
      </div>
    </article>
  </section>
<?php
include('includes/foot.php');
?>
