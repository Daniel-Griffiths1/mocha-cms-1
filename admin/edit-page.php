<?php
/*
* Title: Edit Page Screen
* Developer: Daniel Griffiths
* Updated: 04/03/2015
*/

//enable froala
$froala = 1;

//include main site settings
include('includes/head.php');
include('includes/side.php');
?>

<section id="editor">
  <article>
    <h2>Edit existing page</h2>
    <?php $cms->getEditPage(); ?>
  </article>
</section>
<?php
include('includes/foot.php');
?>
