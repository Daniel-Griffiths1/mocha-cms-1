<?php
/*
* Title: Navigation Editor
* Developer: Daniel Griffiths
* Updated: 27/02/2015
*/

//enable jQuery UI
$jui = 1;

//include main site settings
include('includes/head.php');
include('includes/side.php');
?>
<section>
  <article>
    <h2>Current Navigation</h2>
    <p>*Click and drag nav items to re-order</p>
    <ul id="sortable" class="sortable-connection">
      <?php $cms->getNav(); ?>
    </ul>
    <p>Drop items here to remove them from the navigation</p>
    <ul id="sortable-remove" class="sortable-connection">
    </ul>
    <div id="save-navigation" class="btn">Save Menu</div>
    <div class="clear"></div>
  </article>
</section>
<?php
include('includes/foot.php');
?>
