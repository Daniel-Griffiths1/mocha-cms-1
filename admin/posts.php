<?php
/*
* Title: List Blog Posts
* Developer: Daniel Griffiths
* Updated: 8/03/2015
*/

//include main site settings
include('includes/head.php');
include('includes/side.php');
?>
<section id="editor">
  <article>
    <h2>Current Posts</h2>
    <a href="new-post.php"><div class="btn no-margin"><i class="fa fa-plus"></i> Create New Post</div></a>
    <div class="clear"></div>
    <ul class="posts">
      <?php $cms->getPosts(); ?>
    </ul>
  </article>
</section>
<?php
include('includes/foot.php');
?>
