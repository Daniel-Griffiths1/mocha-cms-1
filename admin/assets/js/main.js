/*
* Title: CMS Main Javascript
* Developer: Daniel Griffiths
*/

//start document ready
$(document).ready(function() {

/*
|-------------------------------------------------------
| Add Fastclick handler (removes 300ms delay on mobile)
|-------------------------------------------------------
*/

  FastClick.attach(document.body);

/*
|-------------------------------------------------------
| Responsive Menu
|-------------------------------------------------------
*/

  $(".res-menu > .fa-bars").click(function() {
    $("nav ul").toggle();
    $("aside").slideToggle( "fast", function() {
      // Animation complete.
    });
  });
  
/*
|-------------------------------------------------------
| Toast Notifactions
|-------------------------------------------------------
*/

function toast(text, color){

  //remove existing toast popups
  $(".toast-notifaction").remove();

  //if colour is defined then use a custom color
  if (!color){
    $("body").append("<div class='toast-notifaction'>" + text + "</div>");
  } else {
    $("body").append("<div style='background:" + color + "' class='toast-notifaction'>" + text + "</div>");
  }

  //add remove toast click handler
  $(".toast-notifaction").click(function() {
    $(this).hide();
  });

  //fadein the new toast popup
  $(".toast-notifaction").fadeIn("fast").delay(5000).fadeOut();;
}

/*
|-------------------------------------------------------
| Covert Text to slug
|-------------------------------------------------------
*/

function convertToSlug(Text){
	return Text
	.toLowerCase()
	.replace(/[^\w ]+/g,'')
	.replace(/ +/g,'-');
}

/* automatically update the slug on text change */
$("#upload-post-form #upload-post-title, #upload-page-form #upload-page-title").on('input',function(){
	var slugText = convertToSlug($(this).val());
	$("#upload-post-filename, #upload-page-filename").val(slugText);
});

/*
|-------------------------------------------------------
| Screen: Pages
|-------------------------------------------------------
*/

  $("#delete-pages").click(function(e) {
    e.preventDefault();

    //create an array from the selected checkboxes
    var checkeditems = $('.page-check:checked')
    .map(function() { return $(this).val() })
    .get()
    .join(",");

    //check if any were selected
    if(checkeditems.length > 1){
      if(confirm("Are you sure? These pages will be deleted forever!")){
        //remove selected pages
        $.post("includes/core/pages/remove-page.php", {
          checkbox: checkeditems,
        }).done(function(data) {
          toast(data,"#DB3B3B");
          //remove row
          $(".page-check:checked").closest("tr").remove();
        });
      }
    } else {
      toast("No pages were selected!","#DB3B3B");
    }
  });

/*
|-------------------------------------------------------
| Screen: New Page
|-------------------------------------------------------
*/

  //upload new webpage on submit
  $("#upload-page-form").on('submit', function(e){
    e.preventDefault(); //prevent form from submiting
    $.post("includes/core/pages/upload-page.php", {
      title: $("#upload-page-title").val(),
      filename: $("#upload-page-filename").val(),
      content: $(".upload-page-content").val()
    }).done(function(data) {
      toast(data);
    });
  });

  //replace spaces in the slug
  $(".slug").keyup(function() {
    var text = $(this).val();
    $(this).val(
      text.replace(/[^a-z0-9\s]/gi, '-').replace(/[_\s]/g, '-')
    );
  });

/*
|-------------------------------------------------------
| Screen: Edit Page
|-------------------------------------------------------
*/

  //upload new webpage on submit
  $("#edit-page-form").on('submit', function(e){
    e.preventDefault(); //prevent form from submiting
    $.post("includes/core/pages/edit-page.php", {
      title: $("#upload-page-title").val(),
      filename: $("#upload-page-filename").val(),
      content: $(".upload-page-content").val()
    }).done(function(data) {
      toast(data);
    });
  });

  //replace spaces in the slug
  $(".slug").keyup(function() {
    var text = $(this).val();
    $(this).val(
      text.replace(/[^a-z0-9\s]/gi, '-').replace(/[_\s]/g, '-')
    );
  });

/*
|-------------------------------------------------------
| Screen: Posts
|-------------------------------------------------------
*/

  $(".delete-post").click(function(e) {
    e.preventDefault();
    if(confirm("Are you sure? This post will be deleted forever!")){
        //remove selected post
        $.post("includes/core/posts/remove-post.php", {
          post: $(this).data("post"),
        }).done(function(data) {
          toast(data,"#DB3B3B");
        });
        //animation
        $(this).closest("li").fadeTo("fast", 0.00, function(){
          $(this).closest("li").slideUp("slow", function() {
            $(this).closest("li").remove();
          });
        });
      }
  });

/*
|-------------------------------------------------------
| Screen: New Post
|-------------------------------------------------------
*/

  //upload new post on submit
  $("#upload-post-form").on('submit', function(e){
    e.preventDefault(); //prevent form from submiting
    $.post("includes/core/posts/upload-post.php", {
      title: $("#upload-post-title").val(),
      filename: $("#upload-post-filename").val(),
      content: $(".upload-post-content").val()
    }).done(function(data) {
      toast(data);
    });
  });

/*
|-------------------------------------------------------
| Screen: Edit Post
|-------------------------------------------------------
*/

  //upload new webpage on submit
  $("#edit-post-form").on('submit', function(e){
    e.preventDefault(); //prevent form from submiting
    $.post("includes/core/posts/edit-post.php", {
      title: $("#upload-post-title").val(),
      filename: $("#upload-post-filename").val(),
      content: $(".upload-post-content").val()
    }).done(function(data) {
      toast(data);
    });
  });

/*
|-------------------------------------------------------
| Screen: Update Navigation
|-------------------------------------------------------
*/

  if (typeof jQuery.ui != 'undefined') {

    //apply jQuery sortable handler
    $("#sortable, #sortable-remove").sortable({
        placeholder: "ui-state-highlight",
        axis: "y",
        connectWith: ".sortable-connection"
    }).disableSelection();


    /*
    $("#sortable ul, #sortable-remove ul").nestedSortable({
        handle: 'div',
        items: 'li',
        toleranceElement: '> div'
    }).disableSelection();
    */

    //save navigation click
    $("#save-navigation").click(function() {
      //new nav array
      var nav = new Array();
      //each each list item to the nav array
      $('#sortable li').each(function () {
        nav.push($(this).html());
      });
      //update navigation on submit
      $.post('includes/core/navigation/update-nav.php', { nav: nav })
      .done(function( data ) {
        toast(data);
      });
    });

  }

/*
|-------------------------------------------------------
| Screen: Media
|-------------------------------------------------------
*/

  //upload new file on submit
  $("#upload-media-form").on('submit', function(e){
    e.preventDefault(); //prevent form from submiting

    var formData = new FormData($(this)[0]);

    $.ajax({
      url: "includes/core/media/upload-media.php",
      type: 'POST',
      data: formData,
      async: false,
      cache: false,
      contentType: false,
      enctype: 'multipart/form-data',
      processData: false,
      success: function (data) {
       toast(data);
      }
   });
  });


}); //end document ready

/*
|-------------------------------------------------------
| Page Load Time
|-------------------------------------------------------
*/

window.onload = function () {
  var loadTime = window.performance.timing.domContentLoadedEventEnd-window.performance.timing.navigationStart;
  loadTime = ((loadTime/1000) % 60).toFixed(2);
  $(".v").text('Page load time is ' + loadTime);
}
