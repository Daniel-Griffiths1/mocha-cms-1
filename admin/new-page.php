<?php
/*
* Title: New Page Screen
* Developer: Daniel Griffiths
* Updated: 27/02/2015
*/

//enable froala
$froala = 1;

//include main site settings
include('includes/head.php');
include('includes/side.php');
?>
  <section id="editor">
		<article>
			<h2>Create new page</h2>
	    <form action="includes/core/pages/upload-page.php" method="post" id="upload-page-form">
				<input class="text-input" name="title" type="text" placeholder="Page Title" id="upload-page-title" required>
				<input class="text-input slug" name="filename" type="text" placeholder="Page Slug (eg. my-new-website-page)" id="upload-page-filename" required>
				<textarea name="content" id="edit" class="upload-page-content" placeholder="Page Content..." required></textarea>
				<div class="wordcount"></div>
	      <input id="upload-page" class="btn submit" type="submit" value="Publish new page">
	    </form>
		</article>
  </section>
<?php
include('includes/foot.php');
?>
