<?php
/*
* Title: CMS Functions
* Developer: Daniel Griffiths
* Updated: 11/07/2015
*/

namespace Mocha;

/*
|-------------------------------------------------------
| Misc Global Functions
|-------------------------------------------------------
*/

/* returns the string between two tags */
function extractString($string, $start, $end) {
    $ini = strpos($string, $start);
    if ($ini == 0) return "";
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

class cms {

/*
|-------------------------------------------------------
| Screen: Pages
|-------------------------------------------------------
*/

  /* List all website pages */
  public function getPages(){
    $pages = glob('../*.{php}', GLOB_BRACE);

    if($pages != false){
      echo "<table>
              <tr>
                <td>&nbsp;</td>
                <td>Title</td>
                <td>Last Modified</td>
              </tr>";
      foreach($pages as $page){
        $page_formatted = basename($page);
        $page_formatted = str_replace('.php','',$page_formatted);
        echo "<tr>
                <td style='width: 38px;'>
                  <input id='$page_formatted' class='page-check' type='checkbox' name='checkbox[]' value='$page'>
                </td>
                <td>
                  <a href='edit-page.php?page=$page_formatted'>$page_formatted</a>
                </td>
                <td>" . date("d/m/Y H:i:s",filemtime($page)) . "</td>
              </tr>";
      }
      echo "</table>";
    } else {
      echo 'No pages currently exist';
    }
  }

/*
|-------------------------------------------------------
| Screen: Edit Page
|-------------------------------------------------------
*/

  /* grab an existing page, grab the content and load it into the editor */
  public function getEditPage(){
    //TODO Finish this
    if(!empty($_GET['page'])){
      $page = file_get_contents('../' . $_GET['page'] . '.php');

      //get title
      $title = extractString($page,$GLOBALS['page_opening_title_tag'],$GLOBALS['page_closing_title_tag']);

      //get content
      $content = extractString($page,$GLOBALS['page_opening_content_tag'],$GLOBALS['page_closing_content_tag']);

      //display editor
      ?>
      <form action="includes/core/pages/edit-page.php" method="post" id="edit-page-form">
        <input class="text-input" name="title" type="text" placeholder="Page Title" id="upload-page-title" value="<?php echo strip_tags($title); ?>" required>
        <input class="text-input slug" name="filename" type="hidden" placeholder="Page Slug (eg. my-new-website-page)" id="upload-page-filename" value="<?php echo $_GET['page']; ?>" required>
        <textarea name="content" id="edit" class="upload-page-content" placeholder="Page Content..." required><?php echo $content; ?></textarea>
        <input id="upload-page" class="btn submit" type="submit" value="Update page">
      </form>
      <?php
    } else {
      die("<h2>Error - no page specified!</h2>");
    }
  }


/*
|-------------------------------------------------------
| Screen: Posts
|-------------------------------------------------------
*/

public function getPosts(){
    //put all files into an array
    $posts = glob('../posts/*.{php}', GLOB_BRACE);

    if($posts != false){

      //sort by create date
      usort($posts, create_function('$a,$b', 'return filemtime($b) - filemtime($a);'));

      //loop through each file
      foreach($posts as $post){

        $post_formatted = basename($post);
        $post_formatted = str_replace('.php','',$post_formatted);

        //get the file contents
        $post_content = file_get_contents($post);

        echo '<li>';

        $title = extractString($post_content,$GLOBALS['post_opening_title_tag'],$GLOBALS['post_closing_title_tag']);

        $content = extractString($post_content,$GLOBALS['post_opening_content_tag'],$GLOBALS['post_closing_content_tag']);

        //display post
        echo  '<strong>' . strip_tags($title) . '</strong>' .
              '<p>' . strip_tags(substr($content , 0, 300)) . '...</p>

               <a target="_blank" class="btn no-margin" href="../posts/' . $post_formatted . '.php"><i class="fa fa-eye"></i> View Post</a>
               <a class="btn green no-margin" href="edit-post.php?post=' . $post_formatted . '"><i class="fa fa-pencil-square-o"></i> Edit Post</a>
               <a class="btn red no-margin delete-post" data-post="' . $post_formatted . '" href="#"><i class="fa fa-times"></i> Remove Post</a>
               <div class="clear"></div>
            </li>';
      
      }
    } else {
      echo 'No posts currently exist';
    }
  }

/*
|-------------------------------------------------------
| Screen: Edit Post
|-------------------------------------------------------
*/

  /* grab an existing post, grab the content and load it into the editor */
  public function getEditPost(){
    //TODO Finish this
    if(!empty($_GET['post'])){
      $post = file_get_contents('../posts/' . $_GET['post'] . '.php');

      //get title
      $title = extractString($post,$GLOBALS['post_opening_title_tag'],$GLOBALS['post_closing_title_tag']);

      //get content
      $content = extractString($post,$GLOBALS['post_opening_content_tag'],$GLOBALS['post_closing_content_tag']);

      //display editor
      ?>
      <form action="includes/core/posts/edit-post.php" method="post" id="edit-post-form">
        <input class="text-input" name="title" type="text" placeholder="Post Title" id="upload-post-title" value="<?php echo strip_tags($title); ?>" required>
        <input class="text-input slug" name="filename" type="hidden" placeholder="Post Slug (eg. my-new-website-post)" id="upload-post-filename" value="<?php echo $_GET['post']; ?>" required>
        <textarea name="content" id="edit" class="upload-post-content" placeholder="Post Content..." required><?php echo $content; ?></textarea>
        <input id="upload-post" class="btn submit" type="submit" value="Update post">
      </form>
      <?php
    } else {
      die("<h2>Error - no post specified!</h2>");
    }
  }

/*
|-------------------------------------------------------
| Screen: Navigation
|-------------------------------------------------------
*/

  /* create a list of all website pages */
  public function getNav(){
    //create array of all website pages
    $pages = glob('../*.{php}', GLOB_BRACE);

    //int for incrementing the id
    $i = 1;

    //loop through all pages
    foreach($pages as &$page){
      //format the text (just to make it look nicer)
      $page_formatted = str_replace('../','',$page);
      $page_formatted = str_replace('.php','',$page_formatted);

      //echo page as a list item
      echo "<li id='nav-$i' class='ui-state-default'>$page_formatted</li>";
      ++$i;
    }
  }

/*
|-------------------------------------------------------
| Screen: Media
|-------------------------------------------------------
*/

  /* grab all files in the media directory */
  public function getMedia(){
    /* List all files */
    $files = glob('uploads/*');
    foreach($files as &$file){
      /* check if file is an image */
      if(getimagesize($file)){
        echo "<div class='f-preview' style='background:url($file);'>";
      } else {
        echo "<div class='f-preview'><div class='file-extention'>" . pathinfo($file, PATHINFO_EXTENSION) . "</div>";
      }

      echo "<div class='filename'>
              <a href='$file' target='_blank'>" . basename($file) . "<a>
              <p style='font-weight:initial;margin-bottom:0;'>Uploaded: " . date("d/m/Y",filemtime($file)) . "</p></div>
            </div>";
    }
  }

/*
|-------------------------------------------------------
| Screen: Plugins
|-------------------------------------------------------
*/

  //TODO refactor this code

  /* grab all plugins from the plugin directory and display info */
  public function getPlugins(){
    $plugins = glob("plugins/*.php");

    echo "<table>
            <tr>
              <td>Plugin Name</td>
              <td>Description</td>
              <td>Version</td>
              <td>Author</td>
            </tr>";
    foreach ($plugins as $plugin) {
      //echo "$plugin size " . filesize($plugin) . '<br>';
      echo "<tr>";
      $plugin = file($plugin); //return file as an array
      $plugin = array_splice($plugin, 2, 4); //slice array to get the needed lines

      //loop through each line and echo the details
      foreach($plugin as $plugin_details){
        //ugh this is ugly...
        echo "<td>" . str_replace(':','',strstr($plugin_details, ':')) . "</td>";
      }
      echo "</tr>";
    }
    echo "</table>";
  }

}

//auto declare the class
$cms = new cms;
