  <div class="clear"></div>
  <!-- jQuery -->
  <script src="assets/js/libs/jquery-1.11.1.min.js"></script>
  <?php if($jui): ?>
  <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
  <script src="assets/js/libs/jquery.ui.touch-punch.min.js"></script>
  <?php endif; ?>

  <!-- Froala -->
  <?php if($froala): ?>
  <script src="assets/js/froala/froala_editor.min.js"></script>
  <!--[if lt IE 9]>
  <script src="assets/js/froala/froala_editor_ie8.min.js"></script>
  <![endif]-->
  <script src="assets/js/froala/plugins/tables.min.js"></script>
  <script src="assets/js/froala/plugins/lists.min.js"></script>
  <script src="assets/js/froala/plugins/colors.min.js"></script>
  <script src="assets/js/froala/plugins/media_manager.min.js"></script>
  <script src="assets/js/froala/plugins/font_family.min.js"></script>
  <script src="assets/js/froala/plugins/font_size.min.js"></script>
  <script src="assets/js/froala/plugins/block_styles.min.js"></script>
  <script src="assets/js/froala/plugins/video.min.js"></script>
  <script>
  $('#edit').editable({
    inlineMode: false,
    minHeight: 150,
    spellcheck: true,
    imageUploadURL: "includes/core/media/upload-media.php",
    imageUploadParam: "uploadedfile",
    pastedImagesUploadURL: "includes/core/media/upload-media.php",
    imageErrorCallback: function (data) {
      // Bad link.
      if (data.errorCode == 1) {
        console.log(data);
      }

      // No link in upload response.
      else if (data.errorCode == 2) {
        console.log(data);
      }

      // Error during file upload.
      else if (data.errorCode == 3) {
        console.log(data);
      }
    },
    noFollow: true,
    //allowedTags: ["p", "ol", "ul", "li", "img", "a", "strong", "u", "i", "strike"],
    //buttons: ["bold", "italic", "underline", "strikeThrough", "quote", "align", "insertOrderedList", "insertUnorderedList", "sep", "createLink", "insertImage", "insertVideo", "html"],
    plainPaste: true,
    customButtons: {
      // Define custom buttons.
      quote: {
        title: 'Quote',
        icon: {
          type: 'font',
          value: 'fa fa-quote-right'
        },
        callback: function (){
          this.insertHTML('<blockquote>Insert quote here</blockquote>');
          this.focus();
        }
      }
    }

  });
  </script>
  <?php endif; ?>

  <!-- Other -->
  <script src="assets/js/libs/fastclick.min.js"></script>
  <script src="assets/js/main.js"></script>

  <!--[if lt IE 9]>
  <script type="text/javascript" src="assets/js/polyfills/placeholder.min.js"></script>
  <![endif]-->

  <div class="v">Work in progress - v0.1</div>
</body>
</html>
