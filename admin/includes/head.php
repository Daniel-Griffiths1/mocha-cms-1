<?php
session_start();

include('includes/settings.php');

/* check if user is logged in */
if (!isset($_SESSION['logged_in'])){
  header('location:login.php');
  die('Username or Password was incorrect'); 
}

/* include core functions */
include('includes/functions.php');

?>
<!doctype html>
<html lang="en">
<head>
  <title>Mocha CMS | A Content Management System</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="author" content="Daniel Griffiths">
  <meta name="viewport" content="width=device-width, maximum-scale=1.0" />
  <meta name="robots" content="all" />
  <!--[if lt IE 9]>
    <script type="text/javascript" src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <link href="assets/css/old-ie.css" rel="stylesheet" type="text/css">
  <![endif]-->
  <!--[if lte IE 7]>
    <div class="browser-warning"><p>This content management system does not support your browser. <br/ >Please update to a more modern browser!</p></div>
  <![endif]-->
  <link href="assets/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/css/main.css" rel="stylesheet">
  <link rel="shortcut icon" type="image/png" href="assets/img/favicon.ico"/>
  <link href="assets/css/froala/font-awesome.min.css" rel="stylesheet" type="text/css">
  <?php if($froala){ ?>
  <link href="assets/css/froala/froala_editor.min.css" rel="stylesheet" type="text/css">
  <link href="assets/css/froala/froala_style.min.css" rel="stylesheet" type="text/css">
  <?php } ?>
</head>
<body id="top">
  <header class="main-head">
    <div class="res-menu">
      <i class="fa fa-bars"></i>
    </div>
    <nav>
      <ul>
        <li><a><?php echo $branding; ?></a></li>
        <li class="logout"><a href="logout"><i class="fa fa-sign-out"></i> Logout</a></li>
      </ul>
      <div class="clear"></div>
    </nav>
    <div class="clear"></div>
  </header>
