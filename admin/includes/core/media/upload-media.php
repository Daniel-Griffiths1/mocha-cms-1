<?php
/*
* Title: Upload Media
* Developer: Daniel Griffiths
* Updated: 02/08/2015
*/

session_start();

/* check if user is logged in */
if (!isset($_SESSION['logged_in'])){
  die('error');
}

//create random number (just in case we create two files with the same name)
$random_number = str_replace(' ','',microtime());
$random_number = str_replace('.','',$random_number);

$upload_path = "../../../uploads/" . $random_number . basename($_FILES['uploadedfile']['name']);

if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $upload_path)) {
  //echo "The file ".  basename($_FILES['uploadedfile']['name']) . " has been uploaded";
  // Generate response.
  $response = new StdClass;
  $response->link = '/admin/uploads/' . $random_number . $_FILES['uploadedfile']['name'];
  echo json_encode($response);
} else{
  die("Error - A file with this name already exists!");
}

