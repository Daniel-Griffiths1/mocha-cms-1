<?php
/*
* Title: Upload New Page
* Developer: Daniel Griffiths
* Updated: 2/08/2015
*/

session_start();

/* check if user is logged in */
if (!isset($_SESSION['logged_in'])){
  die('error');
}

/*
|-------------------------------------------------------
| Disable Magic Quotes if enabled
|-------------------------------------------------------
*/

if (get_magic_quotes_gpc() == 1){
  $_POST = json_decode(stripslashes(json_encode($_POST, JSON_HEX_APOS)), true);
}

/*
|-------------------------------------------------------
| Declare the variables
|-------------------------------------------------------
*/

$filename = $_POST['filename'] . '.php';
$title    = $_POST['title'];
$content  = $_POST['content'];
$template = file_get_contents('../../../templates/page.php'); //grab the template file

/*
|-------------------------------------------------------
| String replace the tags
|-------------------------------------------------------
*/

$template = str_replace("{TITLE}",$title,$template);
$template = str_replace("{CONTENT}",$content,$template);

/*
|-------------------------------------------------------
| Save output to a file
|-------------------------------------------------------
*/

if(isset($filename) && isset($title) && isset($content)){
    echo "$filename has been updated!";
    //echo $template;   //echo output for debugging
    $file = fopen("../../../../$filename", "w"); //create the new page
    fwrite($file,$template); //write content
    fclose($file); //free up memory
} else {
  die("error - some values were not specified");
}

