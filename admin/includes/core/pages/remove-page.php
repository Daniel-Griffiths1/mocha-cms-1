<?php
/*
* Title: Remove Page
* Developer: Daniel Griffiths
* Updated: 2/08/2015
*/

session_start();

/* check if user is logged in */
if (!isset($_SESSION['logged_in'])){
  die('error');
}

/*
|-------------------------------------------------------
| Create trash directory if it doesn't exist
|-------------------------------------------------------
*/

if (!is_dir("../../../trash/")) {
    mkdir("../../../trash/", 0777, true);
}

/*
|-------------------------------------------------------
| Delete all selected pages
|-------------------------------------------------------
*/

//put selected pages into a array
$pages = explode(",", $_POST['checkbox']);

//die out if no pages were defined
if(count(array_filter($pages)) < 1){
  die("No pages were selected!");
}

//create random number (just in case we delete two files with the same name)
$random_number = str_replace(' ','',microtime());
$random_number = str_replace('.','',$random_number);

//remove pages and put into the trash folder (also add a random  number)
foreach($pages as $page){
  rename('../../../' . $page, '../../../trash/' . $random_number . '-' . basename($page));
}

echo "The selected pages have been deleted";

