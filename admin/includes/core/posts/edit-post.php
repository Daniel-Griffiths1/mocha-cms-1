<?php
/*
* Title: Upload New Page
* Developer: Daniel Griffiths
* Updated: 2/08/2015
*/

session_start();

/* check if user is logged in */
if (!isset($_SESSION['logged_in'])){
  die('error');
}

/* include settings file */
require('../../../includes/settings.php');

/*
|-------------------------------------------------------
| Disable Magic Quotes if enabled
|-------------------------------------------------------
*/

if (get_magic_quotes_gpc() == 1){
  $_POST = json_decode(stripslashes(json_encode($_POST, JSON_HEX_APOS)), true);
}

/*
|-------------------------------------------------------
| Create posts directory if it doesn't exist
|-------------------------------------------------------
*/

if (!is_dir("../../../../$post_directory")) {
    mkdir("../../../../$post_directory", 0777, true);
}

/*
|-------------------------------------------------------
| Declare the variables
|-------------------------------------------------------
*/

$filename     = $_POST['filename'] . '.php';
$title        = $_POST['title'];
$date         = date('Y-m-d');
$date_posted  = date('l jS F Y');
$content      = $_POST['content'];
$template     = file_get_contents('../../../templates/post.php'); //grab the template file

/*
|-------------------------------------------------------
| String replace the tags
|-------------------------------------------------------
*/

$template = str_replace("{TITLE}",$title,$template);
$template = str_replace("{CONTENT}",$content,$template);
$template = str_replace("{DATE}",$date,$template);
$template = str_replace("{DATE_POSTED}",$date_posted,$template);

/*
|-------------------------------------------------------
| Save output to a file
|-------------------------------------------------------
*/

if(isset($filename) && isset($title) && isset($content)){
    echo "$filename has been updated!";
    $file = fopen("../../../../$post_directory/$filename", "w"); //create the new page
    fwrite($file,$template); //write content
    fclose($file); //free up memory
} else {
  die("error - some values were not specified");
}

