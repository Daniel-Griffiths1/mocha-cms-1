<?php
/*
* Title: Remove Page
* Developer: Daniel Griffiths
* Updated: 2/08/2015
*/

session_start();

/* check if user is logged in */
if (!isset($_SESSION['logged_in'])){
  die('error');
}

/* include settings file */
require('../../../includes/settings.php');

/*
|-------------------------------------------------------
| Create trash directory if it doesn't exist
|-------------------------------------------------------
*/

if (!is_dir("../../../trash/")) {
    mkdir("../../../trash/", 0777, true);
}

/*
|-------------------------------------------------------
| Delete all selected pages
|-------------------------------------------------------
*/

$post = $_POST['post'];

//die out if no post was defined
if(empty($post)){
  die("Error - no post was defined");
}

//create random number (just in case we delete two files with the same name)
$random_number = str_replace(' ','',microtime());
$random_number = str_replace('.','',$random_number);

//remove post and put into the trash folder (also add a random number)
$success =   rename("../../../../$post_directory/$post.php", "../../../trash/" . $random_number . "-" . basename($post));

if($success === true){
	echo "The selected post has been deleted";
} else {
	echo "Unable to delete the selected post";
}

