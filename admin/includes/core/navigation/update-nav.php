<?php
/*
* Title: Upload Navigation
* Developer: Daniel Griffiths
* Updated: 2/08/2015
*/

session_start();

/* check if user is logged in */
if (!isset($_SESSION['logged_in'])){
  die('error');
}

/*
|-------------------------------------------------------
| Declare the variables
|-------------------------------------------------------
*/

$nav = $_POST['nav'];

/*
|-------------------------------------------------------
| Create posts directory if it doesn't exist
|-------------------------------------------------------
*/

if (!file_exists('../../../../includes/')) {
    mkdir('../../../../includes/', 0777, true);
}

/*
|-------------------------------------------------------
| Save navigation
|-------------------------------------------------------
*/

if (is_array($nav)){

  $nav_list = "<ul>\n";
  foreach($nav as $nav_item){
    if ($nav_item == 'index'){
      //special case for index
      $nav_list .= '<li><a href="index.php">Home</a></li>' . "\n";
    } else {
      $nav_list .= '<li><a href="' . $nav_item . '.php">' . $nav_item . "</a></li>\n";
    }
  }
  $nav_list .= "</ul>";

  $file = fopen("../../../../includes/nav.php", "w"); //create the new page
  fwrite($file,$nav_list); //write content
  fclose($file); //free up memory

  echo 'Navigation has been updated!';
} else {
  die('Error - No nav items were defined');
}
