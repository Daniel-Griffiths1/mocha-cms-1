<?php
//get current page (used for active tabs)
$page = basename($_SERVER['PHP_SELF']);
?>

<aside>
  <ul>
    <li><a target="_blank" href="../"><i class="fa fa-globe"></i> Visit Site</a></li>
    <li><a <?php if ($page == "index.php"){ echo "class='active'";} ?> href="index.php"><i class="fa fa-home"></i> Dashboard</a></li>
    <li><a <?php if ($page == "posts.php" || $page == "new-post.php" || $page == "edit-post.php"){ echo "class='active'";} ?> href="posts.php"><i class="fa fa-newspaper-o"></i> Posts</a></li>
    <li><a <?php if ($page == "pages.php" || $page == "new-page.php" || $page == "edit-page.php"){ echo "class='active'";} ?> href="pages.php"><i class="fa fa-file-word-o"></i> Pages</a></li>
    <li><a <?php if ($page == "navigation.php"){ echo "class='active'";} ?> href="navigation.php"><i class="fa fa-compass"></i> Navigation</a></li>
    <li><a <?php if ($page == "media.php"){ echo "class='active'";} ?> href="media.php"><i class="fa fa-camera"></i> Media</a></li>
    <li><a <?php if ($page == "plugin-list.php"){ echo "class='active'";} ?> href="plugin-list.php"><i class="fa fa-plug"></i> Plugins</a></li>
  </ul>
</aside>
