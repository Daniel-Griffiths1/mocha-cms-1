<?php
/*
|-------------------------------------------------------
| CMS Login Details
|-------------------------------------------------------
*/

$username = 'admin';
$password = 'mochacms';

/*
|-------------------------------------------------------
| Post Details
|-------------------------------------------------------
*/

$post_directory = 'posts';

/*
|-------------------------------------------------------
| CMS Tags
|-------------------------------------------------------
| These dont usually need to be changed.
|-------------------------------------------------------
*/

/* pages */
$page_opening_title_tag 	= '<h2 id="title">';
$page_closing_title_tag 	= '</h2>';
$page_opening_content_tag	= '<div id="content">';
$page_closing_content_tag 	= '</div>';

/* posts */
$post_opening_title_tag 	= '<h2>';
$post_closing_title_tag 	= '</h2>';
$post_opening_content_tag 	= '<div id="content">';
$post_closing_content_tag 	= '</div>';
$post_opening_time_tag 		= '<time>';
$post_closing_time_tag 		= '</time>';

/*
|-------------------------------------------------------
| Other Settings
|-------------------------------------------------------
*/

/*
this will be displayed as plain html this can be
plain text, an image or anything inbetween.
*/
$branding = '<i class="fa fa-coffee"></i> Mocha CMS';


