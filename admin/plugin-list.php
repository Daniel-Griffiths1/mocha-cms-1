<?php
/*
* Title: CMS Plugin Manager
* Developer: Daniel Griffiths
* Updated: 28/02/2015
*/

//include main site settings
include('includes/head.php');
include('includes/side.php');
?>
  <section>
    <article>
      <h2>Plugins</h2>
      <?php $cms->getPlugins(); ?>
    </article>
  </section>
<?php include('includes/foot.php'); ?>
