<?php
/*
* Title: Logout
* Developer: Daniel Griffiths
* Updated: 27/02/2015
*/

//initialize the session
session_start();

//unset all of the session variables
$_SESSION = array();

//if it's desired to kill the session, also delete the session cookie
//note: This will destroy the session, and not just the session data!
if (ini_get("session.use_cookies")) {
  $params = session_get_cookie_params();
  setcookie(session_name(), '', time() - 42000,
  $params["path"], $params["domain"],
  $params["secure"], $params["httponly"]
);
}

//finally, destroy the session.
session_destroy();

//redirect to the login page
header('location:login.php');
?>
