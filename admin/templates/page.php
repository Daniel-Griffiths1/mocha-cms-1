<?php include('includes/head.php'); ?>
<main>
  <section>
    <article>
      <header>
        <h2 id="title">{TITLE}</h2>
      </header>
      <div id="content">{CONTENT}</div>
    </article>
  </section>
</main>
<?php include('includes/foot.php'); ?>
