-Template Info-

Mocha uses template files to create new content. There are two
main types of template files, page and post files.

As you can imagine the “page” template is used when generating
new pages and the “post” template is used when making new posts.

Mocha looks for certain tags in order to insert content into your pages,
please see the full list of tags below:-

-Page Tags-

{TITLE}	   		(Main page title)
{CONTENT}  		(Main page content)

-Post Tags-

{TITLE}	   		(Main post title)
{CONTENT}  		(Main post content)
{DATE}			(Timestamp which can be inserted into a <time> element)
{DATE_POSTED}	(Date when the article was published)

Once a page/post has been created Mocha will then need to do a bit of regex 
to determine where the content is in the HTML markup. You can specify this in
the “/includes/settings.php” file.

I hope than all makes sense! :)