<?php include('../includes/head.php'); ?>
  <main>
    <section>
      <article>
        <header>
          <h2>{TITLE}</h2>
          <time datetime="{DATE}">{DATE_POSTED}</time>
        </header>
        <div id="content">{CONTENT}</div>
        <div id="disqus_thread"></div>
        <script type="text/javascript">
            /* * * CONFIGURATION VARIABLES * * */
            var disqus_shortname = 'example-name';

            /* * * DON'T EDIT BELOW THIS LINE * * */
            (function() {
                var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
            })();
        </script>
        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
      </article>
    </section>
  </main>
<?php include('../includes/foot.php'); ?>
