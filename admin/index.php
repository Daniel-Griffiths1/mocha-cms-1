<?php
/*
* Title: CMS Homepage (dashboard)
* Developer: Daniel Griffiths
* Updated: 27/02/2015
*/

//include main site settings
include('includes/head.php');
include('includes/side.php');
?>
  <section id="editor">
    <article>
      <h2>Dashboard</h2>
      <div class="widget">
        <h3>Weather Forecast</h3>
        <p>
          <?php
          $weather_json = file_get_contents("http://api.openweathermap.org/data/2.5/weather?q=Bridgend,uk");
          $weather_json = json_decode($weather_json, true);
          echo $weather_json['weather']['0']['main'] . ' - ' . $weather_json['weather']['0']['description'];
          ?>
        </p>
      </div>
      <div class="widget">
        <h3>Analytics</h3>
        <div id="embed-api-auth-container"></div>
        <div id="chart-container"></div>
        <div id="view-selector-container" style="display:none;"></div>
      </div>
    </article>
  </section>
<?php
include('includes/foot.php');
?>
<script>
  (function(w,d,s,g,js,fs){
    g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
    js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
    js.src='https://apis.google.com/js/platform.js';
    fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
  }(window,document,'script'));
</script>
<script>

gapi.analytics.ready(function() {

  /**
   * Authorize the user immediately if the user has already granted access.
   * If no access has been created, render an authorize button inside the
   * element with the ID "embed-api-auth-container".
   */
  gapi.analytics.auth.authorize({
    container: 'embed-api-auth-container',
    clientid: '725293464201-rkljq9sj54vcaju1ndh9oeqbuese0493.apps.googleusercontent.com',
  });


  /**
   * Create a new ViewSelector instance to be rendered inside of an
   * element with the id "view-selector-container".
   */
  var viewSelector = new gapi.analytics.ViewSelector({
    container: 'view-selector-container'
  });

  // Render the view selector to the page.
  viewSelector.execute();


  /**
   * Create a new DataChart instance with the given query parameters
   * and Google chart options. It will be rendered inside an element
   * with the id "chart-container".
   */
  var dataChart = new gapi.analytics.googleCharts.DataChart({
    query: {
      metrics: 'ga:sessions',
      dimensions: 'ga:date',
      'start-date': '30daysAgo',
      'end-date': 'yesterday'
    },
    chart: {
      container: 'chart-container',
      type: 'LINE',
      options: {
        width: '100%'
      }
    }
  });


  /**
   * Render the dataChart on the page whenever a new view is selected.
   */
  viewSelector.on('change', function(ids) {
    dataChart.set({query: {ids: ids}}).execute();
  });

});
</script>
</body>
</html>
