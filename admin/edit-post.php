<?php
/*
* Title: Edit Page Screen
* Developer: Daniel Griffiths
* Updated: 08/03/2015
*/

//enable froala
$froala = 1;

//include main site settings
include('includes/head.php');
include('includes/side.php');
?>

<section id="editor">
  <article>
    <h2>Edit existing post</h2>
    <?php $cms->getEditPost(); ?>
  </article>
</section>
<?php
include('includes/foot.php');
?>
