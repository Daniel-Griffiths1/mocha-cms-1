<?php
/*
* Title: New post Screen
* Developer: Daniel Griffiths
* Updated: 27/02/2015
*/

//enable froala
$froala = 1;

//include main site settings
include('includes/head.php');
include('includes/side.php');
?>
<section id="editor">
  <article>
    <h2>Create new post</h2>
    <form action="includes/core/posts/upload-post.php" method="post" id="upload-post-form">
      <input class="text-input" name="title" type="text" placeholder="Post Title" id="upload-post-title" required>
      <input class="text-input slug" name="filename" type="text" placeholder="Post Slug (eg. my-new-website-post)" id="upload-post-filename" required>
      <textarea name="content" id="edit" class="upload-post-content" placeholder="Post Content..." required></textarea>
      <div class="wordcount"></div>
      <input id="upload-post" class="btn submit" type="submit" value="Publish new post">
    </form>
  </article>
</section>
<?php
include('includes/foot.php');
?>
